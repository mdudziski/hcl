const angular = require('angular');

const hclInterviewTask = angular.module('hclInterviewTask', []);


hclInterviewTask.directive('numberField', require('./src/numberField.directive'));
hclInterviewTask.service('numberFieldService', require('./src/numberField.service'));