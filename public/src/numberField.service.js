module.exports = ['$http', '$q', function ($http, $q) {

    this.submitNumber = function (number) {

        return $q(function (resolve, reject) {
            
            $http({
                method: 'POST',
                url: '/intent',
                params: { number: number }
            })
                .then(function (response) {

                    if (response.data.success) {

                        resolve();
                    } else {

                        reject(response.data.error);
                    }

                }, function (error) {

                    console.log('Communication error', error);
                });
        });        
    };
}];