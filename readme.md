# HCL Interview task - Maciej Dudzinski

## Running application

`$ npm install`

`$ npm start` or:

To build static bundle.js: `cd public && ../node_modules/.bin/webpack`

To run backend: `cd server && node app.js`

To run unit tests: `npm test`

Application can be accessed at "http://localhost:3000"

## Implementation details

### Backend

I used express-static middleware to serve all static resources together with bundle.js

Backend also exposes endpoint /intent that is used for submitting form with number.
Backend keeps track of users IDs (their IPs) and the number of tries in In-memory database with a filesystem support to
preserve state after closing node.js process.

Solution with proper database, especially using fast key:value storage like Riak or Redis would be a better solution,
but the implementation would exceed 4 hours of my time.
I used synchronous fs.read function that's a serious performance issue, but as I said, in the real solution it should
be replaced with an async call to database.

### Frontend

On the front-end side I used webpack to generate bundle file with js application.

