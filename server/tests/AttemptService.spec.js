const AttemptService = require('../AttemptService');

describe('AttemptService', function() {

    it('Can be constructed', function() {

        expect(new AttemptService()).toBeDefined();
    });

    it('Contains required methods', function() {

        var service = new AttemptService();

        expect(service.getUserAttempts).toBeDefined();
        expect(service.getUserTriesLeft).toBeDefined();
        expect(service.logAttempt).toBeDefined();
    });

    it('Can log attempts and return correct left number', function() {

        var service = new AttemptService(3);

        expect(service.getUserAttempts('ID')).toBe(0);
        expect(service.getUserTriesLeft('ID')).toBe(3);

        service.logAttempt('ID');

        expect(service.getUserAttempts('ID')).toBe(1);
        expect(service.getUserTriesLeft('ID')).toBe(2);

        service.logAttempt('ID');
        service.logAttempt('ID');

        expect(service.getUserAttempts('ID')).toBe(3);
        expect(service.getUserTriesLeft('ID')).toBe(0);
    });
    it('Can log attempts for different ids', function() {

        var service = new AttemptService(3);

        service.logAttempt('ID');
        service.logAttempt('ID2');
        service.logAttempt('ID2');

        expect(service.getUserAttempts('ID')).toBe(1);
        expect(service.getUserAttempts('ID2')).toBe(2);
    });
});