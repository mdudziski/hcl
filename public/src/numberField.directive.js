module.exports = ['numberFieldService', function (numberFieldService) {

    return {
        restrict: 'E',
        template: `
            <form name="numberField" ng-submit="submit()">
                <label>
                    Insert secret number between {{from}} and {{to}}:
                    <input type="number" min="{{from}}" max="{{to}}" ng-model="number" required="required"/>
                </label>
                <div class='error' ng-show="!numberField.$valid">
                    Given number is out of the range.
                </div>
                
                <div class='error' ng-show="serverError">
                    {{serverError}}
                </div>
                
                <div class="success" ng-show="successSubmit">
                    The number is correct. Good job.
                </div>
                
                <div>
                    <input type="submit" ng-disabled="!numberField.$valid">
                </div>
            </form>
        `,
        link: function (scope, attr) {

            scope.from = +attr.from || 0;
            scope.to = +attr.to || 10;
            scope.number = 0; //default input value

            scope.serverError = null;

            scope.submit = function () {

                numberFieldService.submitNumber(scope.number).then(function () {

                    scope.successSubmit = true;
                    scope.serverError = null;
                }, function (error) {

                    scope.serverError = error;
                    scope.successSubmit = null;
                });
            }
        }
    }
}];