const fs = require('fs');

/**
 * AttemptService class for tracking submitted numbers.
 *
 * @param maxAttempts {Number} Maximum number of tries - used to calculate tries left
 * @param [persistentData] {Boolean} Should filesystem be used to save attempts data
 *
 * @returns {{getUserAttempts: getUserAttempts, getUserTriesLeft: getUserTriesLeft, logAttempt: logAttempt}}
 * @construct
 */
module.exports = function AttemptService (maxAttempts, persistentData) {

    maxAttempts = maxAttempts || 3;

    const dataFile = 'db.json';

    const usersAttempts = persistentData ? JSON.parse(fs.readFileSync(dataFile)) : {};

    const getUserAttempts = function (userId) {

        return usersAttempts[userId] || 0;
    };

    const saveDataToFile = function () {

        fs.writeFile(dataFile, JSON.stringify(usersAttempts));
    };

    return {
        getUserAttempts: getUserAttempts,

        getUserTriesLeft: function (userId) {

            return maxAttempts - getUserAttempts(userId);
        },

        logAttempt: function (userId) {

            usersAttempts[userId] = usersAttempts[userId] || 0;
            usersAttempts[userId] += 1;

            if (persistentData) {

                saveDataToFile();
            }
        }
    }
};