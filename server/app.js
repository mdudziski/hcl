var express = require('express');
var AttemptService = require('./AttemptService');

var app = express();

var secretNumber = 7;
var maxAttempts = 3;

//Service storing submit attempts for all users
var attemptService = new AttemptService(maxAttempts, true);

//Serve all static content like index.html and js files
app.use(express.static('../public'));

//Intent endpoint used for tracking submit attempts
app.post('/intent', function (req, res) {

    var submittedNumber = req.query.number;

    //We are using user IP address as the user's unique id
    var userId = req.connection.remoteAddress;

    var triesLeft = attemptService.getUserTriesLeft(userId);

    if (triesLeft === 0) {

        res.json({ error: 'Maximum attempts reached.'})
    } else {

        if (+submittedNumber === +secretNumber) {

            res.json({ success: true });
        } else {

            attemptService.logAttempt(userId);
            res.json({ error:
                `Incorrect number. 
                Attempts: ${attemptService.getUserAttempts(userId)}.
                Tries left: ${attemptService.getUserTriesLeft(userId)}.`
            })
        }

    }

});

app.listen(3000);